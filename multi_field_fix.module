<?php

/**
 * @file 
 *   In a very basic way, allows fields with multiple values allowed, to use the unlimited field, 
 *   with a maximum set too. This basically means the forms can be shorter in length. 
 */

/**
 * Implements hook_form_alter()
 * 
 * @param array $form
 * @param array $form_state
 * @param string $form_id 
 */
function multi_field_fix_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == "page_node_form") {
    // server validation
    $form['#validate'][] = 'multi_field_fix_validate_repeating_fields';
    $module_path = drupal_get_path("module", "multi_field_fix");
    
    // @todo try do some live validation here if possible
    drupal_add_js("$module_path/js/multi_field_fix-validation.js", array('group' => JS_LIBRARY, "weight", "-10"));
  }
  elseif ($form_id == "field_ui_field_edit_form") {
    // Adjust and add the settings to the field edit form.
    $form['field']['cardinality']['#weight'] = -2;
    $form['field']['cardinality']['#title'] = t("Number of Fields to collect");
    $form['field']['cardinality']['#description'] .= "<br />If you set this to unlimited you'll then be able to set a maximum number of elements here too.";
    
    // We only display this when the user selects Unlimited in cardinality.
    $form['field']["multi_field_fix_{$form['#field']['field_name']}"] = array(
      '#weight' => -1,
      '#type' => 'select',
      '#title' => "Maximum No. of Fields (if set to unlimited above)",
      '#description' => 'This setting combined with the value being set to \'Unlimited\' above, will restrict the field number, but only display the form fields if required.',
      '#options' => array(FIELD_CARDINALITY_UNLIMITED => t('Unlimited')) + drupal_map_assoc(range(1, 10)),
      '#default_value' => variable_get("multi_field_fix_{$form['#field']['field_name']}", -1),
      '#states' => array(
        'invisible' => array(
          ':input[name="field[cardinality]"]' => array('!value' => "-1"),
        ),
      ),
    );
      
    $form['#submit'][] = "multi_field_fix_form_content_settings_submit";
  }
}


/**
 * Custom submit handler for field settings save.
 * @param array $form
 * @param array $form_state 
 */
function multi_field_fix_form_content_settings_submit($form, &$form_state) {
  if ($form['#form_id'] == "field_ui_field_edit_form") {
    if (isset($form_state['values']['field'])) {
      variable_set("multi_field_fix_{$form['#field']['field_name']}", $form_state['values']['field']["multi_field_fix_{$form['#field']['field_name']}"], "-1");
    }
  }
}


/**
 * Custom validation handler for field edit forms.
 * Finds the real number of fields with values set.
 * @param array $form
 * @param array $form_state 
 */
function multi_field_fix_validate_repeating_fields($form, &$form_state) {
  
  foreach ($form as $key => $value) {
    if (strpos($key, "field_") !== FALSE) {
      $real_count = 0;
      $maximum = variable_get("multi_field_fix_{$key}", -1);
      
      if ($maximum != -1) { // Only do our checks if we have a maximum set.
        for ($i=0; $i<= $form[$key][$form[$key]['#language']]['#max_delta']; $i++) {
          if (isset($form[$key][$form[$key]['#language']][$i])) {
            if (isset($form[$key][$form[$key]['#language']][$i]['value']['#value'])) {
              $field_value = $form[$key][$form[$key]['#language']][$i]['value']['#value'];
              if ($field_value != "" && $field_value != FALSE) { // may have cleared out the values
                ++$real_count;
              }
            }
          }
        }

        if ($real_count > $maximum) {
          form_set_error($key, t("You may only have a maximum of @maximum elements for this field. Please clear out the others.", array('@maximum' => $maximum)));
        }
      }
    }
  }
}


